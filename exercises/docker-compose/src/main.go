package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	log.Printf("db host: %s\n", os.Getenv("DB_HOST"))

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		log.Printf("\n%s %s\n%s", req.Method, req.Host, req.Header)
		w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
		w.Write([]byte("Hello world"))
	})

	log.Println("server was started")
	defer log.Println("server was stopped")

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err.Error())
	}
}
