#!/bin/bash

curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs
cd /vagrant
npm init -y fastify
npm install
FASTIFY_ADDRESS=0.0.0.0 npm run dev
